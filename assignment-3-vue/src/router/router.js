import TriviaStartPage from '../components/StartPage/TriviaStartPage.vue'
import Question from '../components/QuestionsPage/Question.vue'
import Result from '../components/ResultPage/Result.vue'
import Vue from "vue";
import VueRouter from "vue-router"

Vue.use(VueRouter)

const triviaRoutes = [
    {
        path: '/',
        component: TriviaStartPage
    },
    {
        path: '/questions',
        component: Question
    }
    ,
    {
        path: '/result',
        component: Result
    }
]
// not needed if using routes isted of triviaRoutes
const router = new VueRouter({routes: triviaRoutes})

export default router