export const getAllCategories = () => {
  return fetch("https://opentdb.com/api_category.php")
    .then((response) => response.json())
    .then((data) => data);
};

export const getNumberOfQuestionsForGivenCategory = (CATEGORY_ID_HERE) => {
  return fetch(`https://opentdb.com/api_count.php?category=${CATEGORY_ID_HERE}`)
    .then((response) => response.json())
    .then((data) => data);
};


export const getRequestedTriviaQuestions = (AMOUNT, CATEGORY, DIFFICULTY) => {
    return fetch(`https://opentdb.com/api.php?amount=${AMOUNT}&category=${CATEGORY}&difficulty=${DIFFICULTY}`)
        .then(response =>  response.json())
        .then(data => {
            data.results.forEach(e => {
                e.question = HtmlToTextDecoder(e.question)
            });
            data.results.forEach(e => {
                e.correct_answer = HtmlToTextDecoder(e.correct_answer)
            });  
            data.results.forEach(e => {
                for (let i = 0; i < e.incorrect_answers.length; i++) {
                    e.incorrect_answers[i] = HtmlToTextDecoder(e.incorrect_answers[i])  
                }
            });             
            return data
         })             
}

function HtmlToTextDecoder(data){
    const n = document.createElement('textarea')
    n.innerHTML = data;

    return data = n.value;
}
