import Vue from "vue";
import Vuex from "vuex";
import {
  getAllCategories,
  getRequestedTriviaQuestions,
  getNumberOfQuestionsForGivenCategory,
} from "../util/triviaAPI";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    //New every time
    categories: [],
    //Needs a reset
    category: 22,
    difficulty: "easy",
    numberOfQuestions: 10,
    requestedQuestions: [],
    submittedAnswers: [],
    currentQuestionIndex: 0,
    correctAnswers: 0,
    //Resets itself
    questionAlternatives: [],
    error: "",
    currentQuestionName: "",
    maxNumberOfQuestions: 0,
  },
  mutations: {
    setCategories: (state, payload) => {
      state.categories = payload;
    },
    setCategory: (state, payload) => {
      state.category = payload;
    },
    setError: (state, payload) => {
      state.error = payload;
    },
    setDifficulty: (state, payload) => {
      state.difficulty = payload;
    },
    setNumberOfQuestions: (state, payload) => {
      state.numberOfQuestions = payload;
    },
    setRequestedQuestions: (state, payload) => {
      state.requestedQuestions = payload;
    },
    setQuestionAlternatives: (state, payload) => {
      state.questionAlternatives = payload;
    },
    setCurrentQuestionIndex: (state, payload) => {
      state.currentQuestionIndex = payload;
    },
    setCorrectAnswers: (state, payload) => {
      state.correctAnswers = payload;
    },
    setSubmittedAnswers: (state, payload) => {
      state.submittedAnswers = payload;
    },
    setReset: (state) => {
      (state.category = 22),
        (state.difficulty = "easy"),
        (state.numberOfQuestions = 10),
        (state.requestedQuestions = []);
      state.submittedAnswers = [];
      state.currentQuestionIndex = 0;
      state.correctAnswers = 0;
    },
    setMaxNumberOfQuestions: (state, payload) => {
      state.maxNumberOfQuestions = payload;
    },
  },
  actions: {
    async getCategories({ commit }) {
      try {
        const categories = await getAllCategories();
        commit("setCategories", categories.trivia_categories);
      } catch (e) {
        commit("setError", e.message);
      }
    },
    async getQuestions({ commit, state }) {
      try {
        const quesitons = await getRequestedTriviaQuestions(
          state.numberOfQuestions,
          state.category,
          state.difficulty
        );
        commit("setRequestedQuestions", quesitons.results);
      } catch (e) {
        commit("setError", e.message);
      }
    },
    async getNumberOfQuestionsForGivenCategory({ commit, state }) {
      try {
        const maxNumberOfQuestions = await getNumberOfQuestionsForGivenCategory(
          state.category
        );
        if (state.difficulty == "easy") {
          commit(
            "setMaxNumberOfQuestions",
            maxNumberOfQuestions.category_question_count
              .total_easy_question_count
          );
        } else if (state.difficulty == "medium") {
          commit(
            "setMaxNumberOfQuestions",
            maxNumberOfQuestions.category_question_count
              .total_medium_question_count
          );
        } else if (state.difficulty == "hard") {
          commit(
            "setMaxNumberOfQuestions",
            maxNumberOfQuestions.category_question_count
              .total_hard_question_count
          );
        }
      } catch (e) {
        commit("setError", e.message);
      }
    },
    updateQuestionAlternatives({ commit, state }) {
      let tempArrr = JSON.parse(
        JSON.stringify(
          state.requestedQuestions[state.currentQuestionIndex].incorrect_answers
        )
      );
      tempArrr.push(
        state.requestedQuestions[state.currentQuestionIndex].correct_answer
      );
      tempArrr.sort(() => Math.random() - 0.5);
      commit("setQuestionAlternatives", tempArrr);
    },
    incrementQuestionIndex({ commit, state }) {
      let tempIndex = state.currentQuestionIndex;
      tempIndex += 1;
      commit("setCurrentQuestionIndex", tempIndex);
    },
    incrementCorrectAnswers({ commit, state }) {
      let tempPoints = state.correctAnswers;
      tempPoints += 10;
      commit("setCorrectAnswers", tempPoints);
    },
    addSubmittedAnswer({ commit, state }, payload) {
      let tempArr = state.submittedAnswers;
      tempArr.push(payload);
      console.log("answer" + tempArr);
      commit("setSubmittedAnswers", tempArr);
    },
    resetEverything({ commit }) {
      commit("setReset");
    },
  },
  getters: {
    getterCategories: (state) => {
      return state.categories;
    },
    getterQuestions: (state) => {
      return state.questionAlternatives;
    },
    getterQuestionType: (state) => {
      return state.requestedQuestions[state.currentQuestionIndex].type;
    },
    getterCorrectAnswer: (state) => {
      return state.requestedQuestions[state.currentQuestionIndex]
        .correct_answer;
    },
    getterCurrentQuestionName: (state) => {
      return state.requestedQuestions[state.currentQuestionIndex].question;
    },
    getterRequestedQuestions: (state) => {
      return state.requestedQuestions;
    },
    getterSubmittedAnswers: (state) => {
      return state.submittedAnswers;
    },
    getterScore: (state) => {
      return state.correctAnswers;
    },
    getterMaxNumberOfQuestions: (state) => {
      return state.maxNumberOfQuestions;
    },
    getterNumberOfQuestions: (state) => {
      return state.numberOfQuestions;
    },
  },
});
